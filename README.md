# Bitbucket Pipelines Pipe: IBM Cloud API to kubeconf 

This pipe generates an (Base64 encoded) kubeconf file for your IBM Cloud Kubernetes service using an IAM# API key. 

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: hgentenaar/ibmcloud-api-to-kubeconf-pipe:0.1.0
    variables:
      API_KEY: "<string>"
      CLUSTER_ID: "<string>"
      OUTPUT_PATH: "<string>"
      # DEBUG: "<boolean>" # Optionali
    artifacts:
      - OUTPUT_PATH/kubeconf

# - next step or pipe 
#   export kubeconf=$(cat OUTPUT_PATH/kubeconf) 
#   # You can now use $kubeconf in yout next step 
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| API_KEY (*)           | The IBM Cloud account API key used to generate a token fetch the kubeconf|
| CLUSTER_ID (*)        | The Cluster ID for the kubeconf
| OUTPUT_PATH (*)       | Output path for the kubeconf file (bse64 encoded)|
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

You need an IAM# API key and the Kuberneres cluster ID for wich you want to generate kubeconf

## Examples

Basic example:

```yaml
script:
  - pipe: hgentenaar/ibmcloud-api-to-kubeconf-pipe:0.1.0
    variables:
      API_KEY: $API_KEY 
      CLUSTER_ID: $CLUSTER_ID
      OUTPUT_PATH: kubecon 
      # DEBUG: "<boolean>" # Optionali
    artifacts:
      - kubecon/kubeconf

script:
  - pipe: docker://csstaylor/helm-deploy:0.1.0
    export KUBE_CONFIG=$(cat kubecon/kubeconf)
    variables:
      KUBE_CONFIG: $KUBE_CONFIG
      HELM_RELEASE_NAME: 'myRedis'
      HELM_COMMAND: 'upgrade'
      HELM_UPGRADE_INSTALL_IF_NOT_PRESENT: 'true'
      HELM_CHART_NAME: 'stable/redis'
```

Advanced example:

```yaml
script:
  - pipe: hgentenaar/ibmcloud-api-to-kubeconf-pipe:0.1.0
    variables:
      NAME: "foobar"
      DEBUG: "true"
```

## Support
I made this pipe to learn how to make custom bitbucket pipes. Even though this is just an example it should still function.
If you’d like help with this pipe, or you have an issue or feature request, let us know.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
